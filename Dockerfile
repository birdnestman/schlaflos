# Use an official Elixir runtime as a parent image
FROM bitwalker/alpine-elixir-phoenix:1.12.3

RUN apk update && \
  apk --no-cache --update add \
    postgresql-client \
    imagemagick && \
    rm -rf /var/cache/apk/*

EXPOSE 4000
ENV PORT=4000

# Create app directory and copy the Elixir projects into it
RUN mkdir /app
COPY . /app
WORKDIR /app

# Install hex package manager
RUN mix local.hex --force
RUN mix local.rebar --force

# Compile the project
RUN mix do compile

# Same with npm deps
ADD assets/package.json assets/
RUN cd assets && \
    npm install

# Run frontend build, compile, and digest assets
RUN cd assets/ && \
    npm run deploy && \
    cd - && \
    mix do compile, phx.digest

CMD ["/app/entrypoint.sh"]
