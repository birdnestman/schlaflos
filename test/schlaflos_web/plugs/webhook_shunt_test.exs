defmodule SchlaflosWeb.Plugs.WebhookShuntTest do
  use ExUnit.Case, async: true
  use Plug.Test
  alias SchlaflosWeb.WebhookRouter

  @opts WebhookRouter.init([])

  test "returns hello world" do
    # Create a test connection
    conn = conn(:post, "/webhook/order.completed", %{})

    # Invoke the plug
    conn = WebhookRouter.call(conn, @opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "{\"status\":\"ok\"}"
  end
end
