defmodule SchlaflosWeb.WebhookControllerTest do
  use SchlaflosWeb.ConnCase

  alias Schlaflos.{Accounts, Memberships}

  describe "create order" do
    setup [:order_completed_response]

    test "with new customer", %{
      conn: conn,
      order_completed_data: order_completed_data
    } do
      assert nil == Accounts.get_user_by_email("mans@huster.com")

      conn = post(conn, "/webhook", order_completed_data)

      assert %Accounts.User{full_name: name, email: email} =
               Accounts.get_user_by_email("mans@huster.com")

      assert name == "Burton Robinson"
      assert email == "mans@huster.com"

      # assert %{id: id} = redirected_params(conn)
      # assert redirected_to(conn) == Routes.page_path(conn, :show, id)

      # conn = get(conn, Routes.page_path(conn, :show, id))
      assert %{"status" => "ok"} == json_response(conn, 200)
    end

    test "with existing customer", %{
      conn: conn,
      order_completed_data: order_completed_data
    } do
      _user = insert(:user, full_name: "Burton Robinson", email: "mans@huster.com")
      conn = post(conn, "/webhook", order_completed_data)

      membership =
        Memberships.get_membership_by_token!(get_in(order_completed_data, ["content", "token"]))

      assert membership.active == true
      assert %{"status" => "ok"} == json_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: _conn} do
      # conn = post(conn, Routes.page_path(conn, :create), page: @invalid_attrs)
      # assert html_response(conn, 200) =~ "New Page"
    end
  end

  describe "create order" do
    setup [:order_completed_response]

    test "with new customer", %{
      conn: conn,
      order_completed_data: order_completed_data
    } do
      assert nil == Accounts.get_user_by_email("mans@huster.com")

      conn = post(conn, "/webhook", order_completed_data)

      assert %Accounts.User{full_name: name, email: email} =
               Accounts.get_user_by_email("mans@huster.com")

      assert name == "Burton Robinson"
      assert email == "mans@huster.com"

      # assert %{id: id} = redirected_params(conn)
      # assert redirected_to(conn) == Routes.page_path(conn, :show, id)

      # conn = get(conn, Routes.page_path(conn, :show, id))
      assert %{"status" => "ok"} == json_response(conn, 200)
    end

    test "with existing customer", %{
      conn: conn,
      order_completed_data: order_completed_data
    } do
      _user = insert(:user, full_name: "Burton Robinson", email: "mans@huster.com")
      conn = post(conn, "/webhook", order_completed_data)

      membership =
        Memberships.get_membership_by_token!(get_in(order_completed_data, ["content", "token"]))

      assert membership.active == true
      assert %{"status" => "ok"} == json_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: _conn} do
      # conn = post(conn, Routes.page_path(conn, :create), page: @invalid_attrs)
      # assert html_response(conn, 200) =~ "New Page"
    end
  end

  describe "change order state" do
    setup [
      :status_change_cancelled_response,
      :status_change_processed_response,
      :status_change_shipped_response
    ]

    test "cancel", %{
      conn: conn,
      status_change_cancelled: status_change_cancelled
    } do
      user = insert(:user, full_name: "Burton Robinson", email: "mans@huster.com")
      membership = insert(:membership, user_id: user.id, active: true, state: "created")

      assert %Accounts.User{id: user_id} = Accounts.get_user_by_email("mans@huster.com")

      conn = post(conn, "/webhook", status_change_cancelled)

      assert result = Memberships.get_membership_by_token!(membership.token)
      assert result.active == false
      assert result.state == "cancelled"
      assert result.user_id == user_id

      assert %{"status" => "ok"} == json_response(conn, 200)
    end

    test "processed", %{
      conn: conn,
      status_change_processed: status_change_processed
    } do
      user = insert(:user, full_name: "Burton Robinson", email: "mans@huster.com")
      membership = insert(:membership, user_id: user.id, active: false, state: "cancelled")

      conn = post(conn, "/webhook", status_change_processed)

      assert result = Memberships.get_membership_by_token!(membership.token)
      assert result.active == true
      assert result.state == "processed"

      assert %{"status" => "ok"} == json_response(conn, 200)
    end

    test "shipped", %{
      conn: conn,
      status_change_shipped: status_change_shipped
    } do
      user = insert(:user, full_name: "Burton Robinson", email: "mans@huster.com")
      membership = insert(:membership, user_id: user.id, active: false, state: "cancelled")

      conn = post(conn, "/webhook", status_change_shipped)

      assert result = Memberships.get_membership_by_token!(membership.token)
      assert result.active == true
      assert result.state == "shipped"

      assert %{"status" => "ok"} == json_response(conn, 200)
    end
  end

  defp order_completed_response(_) do
    response_as_string = File.read!("test/support/snipcart/order_completed.json")
    response_as_map = Jason.decode!(response_as_string)

    %{order_completed_data: response_as_map}
  end

  defp status_change_cancelled_response(_) do
    response_as_string = File.read!("test/support/snipcart/status_change_cancelled.json")
    response_as_map = Jason.decode!(response_as_string)

    %{status_change_cancelled: response_as_map}
  end

  defp status_change_processed_response(_) do
    response_as_string = File.read!("test/support/snipcart/status_change_processed.json")
    response_as_map = Jason.decode!(response_as_string)

    %{status_change_processed: response_as_map}
  end

  defp status_change_shipped_response(_) do
    response_as_string = File.read!("test/support/snipcart/status_change_shipped.json")
    response_as_map = Jason.decode!(response_as_string)

    %{status_change_shipped: response_as_map}
  end
end
