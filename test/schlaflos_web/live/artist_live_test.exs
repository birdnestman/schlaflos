defmodule SchlaflosWeb.ArtistLiveTest do
  use SchlaflosWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Schlaflos.Artists

  @create_attrs %{
    image: "some image",
    name: "some name",
    soundcloud: "some soundcloud",
    website: "some website"
  }
  @update_attrs %{
    image: "some updated image",
    name: "some updated name",
    soundcloud: "some updated soundcloud",
    website: "some updated website"
  }
  @invalid_attrs %{image: nil, name: nil, soundcloud: nil, website: nil}

  defp fixture(:artist) do
    {:ok, artist} = Artists.create_artist(@create_attrs)
    artist
  end

  defp create_artist(_) do
    artist = fixture(:artist)
    %{artist: artist}
  end

  describe "Index" do
    setup [:create_artist]

    test "lists all artists", %{conn: conn, artist: artist} do
      {:ok, _index_live, html} = live(conn, Routes.artist_index_path(conn, :index))

      assert html =~ "Listing Artists"
      assert html =~ artist.image
    end

    test "saves new artist", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.artist_index_path(conn, :index))

      assert index_live |> element("a", "New Artist") |> render_click() =~
               "New Artist"

      assert_patch(index_live, Routes.artist_index_path(conn, :new))

      assert index_live
             |> form("#artist-form", artist: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#artist-form", artist: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.artist_index_path(conn, :index))

      assert html =~ "Artist created successfully"
      assert html =~ "some image"
    end

    test "updates artist in listing", %{conn: conn, artist: artist} do
      {:ok, index_live, _html} = live(conn, Routes.artist_index_path(conn, :index))

      assert index_live |> element("#artist-#{artist.id} a", "Edit") |> render_click() =~
               "Edit Artist"

      assert_patch(index_live, Routes.artist_index_path(conn, :edit, artist))

      assert index_live
             |> form("#artist-form", artist: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#artist-form", artist: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.artist_index_path(conn, :index))

      assert html =~ "Artist updated successfully"
      assert html =~ "some updated image"
    end

    test "deletes artist in listing", %{conn: conn, artist: artist} do
      {:ok, index_live, _html} = live(conn, Routes.artist_index_path(conn, :index))

      assert index_live |> element("#artist-#{artist.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#artist-#{artist.id}")
    end
  end

  describe "Show" do
    setup [:create_artist]

    test "displays artist", %{conn: conn, artist: artist} do
      {:ok, _show_live, html} = live(conn, Routes.artist_show_path(conn, :show, artist))

      assert html =~ "Show Artist"
      assert html =~ artist.image
    end

    test "updates artist within modal", %{conn: conn, artist: artist} do
      {:ok, show_live, _html} = live(conn, Routes.artist_show_path(conn, :show, artist))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Artist"

      assert_patch(show_live, Routes.artist_show_path(conn, :edit, artist))

      assert show_live
             |> form("#artist-form", artist: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#artist-form", artist: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.artist_show_path(conn, :show, artist))

      assert html =~ "Artist updated successfully"
      assert html =~ "some updated image"
    end
  end
end
