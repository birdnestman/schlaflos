defmodule SchlaflosWeb.Admin.EventLiveTest do
  use SchlaflosWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Schlaflos.Events

  @create_attrs %{
    description: "some description",
    end: "2010-04-17T14:00:00Z",
    name: "some name",
    slug: "some slug",
    start: "2010-04-17T14:00:00Z",
    ticket_price: "120.5",
    ticket_url: "some ticket_url"
  }
  @update_attrs %{
    description: "some updated description",
    end: "2011-05-18T15:01:01Z",
    name: "some updated name",
    slug: "some updated slug",
    start: "2011-05-18T15:01:01Z",
    ticket_price: "456.7",
    ticket_url: "some updated ticket_url"
  }
  @invalid_attrs %{
    description: nil,
    end: nil,
    name: nil,
    slug: nil,
    start: nil,
    ticket_price: nil,
    ticket_url: nil
  }

  defp fixture(:event) do
    {:ok, event} = Events.create_event(@create_attrs)
    event
  end

  defp create_event(_) do
    event = fixture(:event)
    %{event: event}
  end

  describe "Index" do
    setup [:create_event]

    test "lists all events", %{conn: conn, event: event} do
      {:ok, _index_live, html} = live(conn, Routes.admin_event_index_path(conn, :index))

      assert html =~ "Listing Events"
      assert html =~ event.description
    end

    test "saves new event", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.admin_event_index_path(conn, :index))

      assert index_live |> element("a", "New Event") |> render_click() =~
               "New Event"

      assert_patch(index_live, Routes.admin_event_index_path(conn, :new))

      assert index_live
             |> form("#event-form", event: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#event-form", event: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.admin_event_index_path(conn, :index))

      assert html =~ "Event created successfully"
      assert html =~ "some description"
    end

    test "updates event in listing", %{conn: conn, event: event} do
      {:ok, index_live, _html} = live(conn, Routes.admin_event_index_path(conn, :index))

      assert index_live |> element("#event-#{event.id} a", "Edit") |> render_click() =~
               "Edit Event"

      assert_patch(index_live, Routes.admin_event_index_path(conn, :edit, event))

      assert index_live
             |> form("#event-form", event: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#event-form", event: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.admin_event_index_path(conn, :index))

      assert html =~ "Event updated successfully"
      assert html =~ "some updated description"
    end

    test "deletes event in listing", %{conn: conn, event: event} do
      {:ok, index_live, _html} = live(conn, Routes.admin_event_index_path(conn, :index))

      assert index_live |> element("#event-#{event.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#event-#{event.id}")
    end
  end

  describe "Show" do
    setup [:create_event]

    test "displays event", %{conn: conn, event: event} do
      {:ok, _show_live, html} = live(conn, Routes.admin_event_show_path(conn, :show, event))

      assert html =~ "Show Event"
      assert html =~ event.description
    end

    test "updates event within modal", %{conn: conn, event: event} do
      {:ok, show_live, _html} = live(conn, Routes.admin_event_show_path(conn, :show, event))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Event"

      assert_patch(show_live, Routes.admin_event_show_path(conn, :edit, event))

      assert show_live
             |> form("#event-form", event: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#event-form", event: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.admin_event_show_path(conn, :show, event))

      assert html =~ "Event updated successfully"
      assert html =~ "some updated description"
    end
  end
end
