defmodule Schlaflos.ArtistsTest do
  use Schlaflos.DataCase

  alias Schlaflos.Artists

  describe "artists" do
    alias Schlaflos.Artists.Artist

    @valid_attrs %{
      image: "some image",
      name: "some name",
      soundcloud: "some soundcloud",
      website: "some website"
    }
    @update_attrs %{
      image: "some updated image",
      name: "some updated name",
      soundcloud: "some updated soundcloud",
      website: "some updated website"
    }
    @invalid_attrs %{image: nil, name: nil, soundcloud: nil, website: nil}

    def artist_fixture(attrs \\ %{}) do
      {:ok, artist} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Artists.create_artist()

      artist
    end

    test "list_artists/0 returns all artists" do
      artist = artist_fixture()
      assert Artists.list_artists() == [artist]
    end

    test "get_artist!/1 returns the artist with given id" do
      artist = artist_fixture()
      assert Artists.get_artist!(artist.id) == artist
    end

    test "create_artist/1 with valid data creates a artist" do
      assert {:ok, %Artist{} = artist} = Artists.create_artist(@valid_attrs)
      assert artist.image == "some image"
      assert artist.name == "some name"
      assert artist.soundcloud == "some soundcloud"
      assert artist.website == "some website"
    end

    test "create_artist/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Artists.create_artist(@invalid_attrs)
    end

    test "update_artist/2 with valid data updates the artist" do
      artist = artist_fixture()
      assert {:ok, %Artist{} = artist} = Artists.update_artist(artist, @update_attrs)
      assert artist.image == "some updated image"
      assert artist.name == "some updated name"
      assert artist.soundcloud == "some updated soundcloud"
      assert artist.website == "some updated website"
    end

    test "update_artist/2 with invalid data returns error changeset" do
      artist = artist_fixture()
      assert {:error, %Ecto.Changeset{}} = Artists.update_artist(artist, @invalid_attrs)
      assert artist == Artists.get_artist!(artist.id)
    end

    test "delete_artist/1 deletes the artist" do
      artist = artist_fixture()
      assert {:ok, %Artist{}} = Artists.delete_artist(artist)
      assert_raise Ecto.NoResultsError, fn -> Artists.get_artist!(artist.id) end
    end

    test "change_artist/1 returns a artist changeset" do
      artist = artist_fixture()
      assert %Ecto.Changeset{} = Artists.change_artist(artist)
    end
  end
end
