defmodule Schlaflos.SnipcartParserTest do
  use ExUnit.Case

  alias Schlaflos.SnipcartParser

  describe "parse_user/1" do
    setup [:order_completed_response]

    test "success: accepts a valid payload, returns a map of structs", %{
      order_completed_data: response_as_map
    } do
      assert %{
               full_name: "Burton Robinson",
               address: "69 South Oak Drive",
               zip: 5200,
               city: "Brugg",
               email: "mans@huster.com"
             } = SnipcartParser.parse_user(response_as_map)
    end
  end

  describe "get_email/1" do
    setup [:order_completed_response]

    test "success: extract email", %{
      order_completed_data: response_as_map
    } do
      assert "mans@huster.com" = SnipcartParser.get_email(response_as_map)
    end
  end

  describe "get_payment_status/1" do
    setup [:order_completed_deferred_response]

    test "success: deferred payment method", %{
      order_completed_data: response_as_map
    } do
      assert "PaidDeferred" = SnipcartParser.get_payment_status(response_as_map)
    end
  end

  describe "has_membership?/1" do
    setup [:order_completed_response]

    test "contains a membership as item", %{
      order_completed_data: response_as_map
    } do
      assert SnipcartParser.has_membership?(response_as_map) == true
    end
  end

  defp order_completed_response(_) do
    response_as_string = File.read!("test/support/snipcart/order_completed.json")
    response_as_map = Jason.decode!(response_as_string)

    %{order_completed_data: response_as_map}
  end

  defp order_completed_deferred_response(_) do
    response_as_string = File.read!("test/support/snipcart/order_completed_deffered_payment.json")
    response_as_map = Jason.decode!(response_as_string)

    %{order_completed_deferred_data: response_as_map}
  end
end
