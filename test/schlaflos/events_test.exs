defmodule Schlaflos.EventsTest do
  use Schlaflos.DataCase

  alias Schlaflos.Events

  describe "events" do
    alias Schlaflos.Events.Event

    @valid_attrs %{
      description: "some description",
      end: "2010-04-17T14:00:00Z",
      name: "some name",
      slug: "some slug",
      start: "2010-04-17T14:00:00Z",
      ticket_price: "120.5",
      ticket_url: "some ticket_url"
    }
    @update_attrs %{
      description: "some updated description",
      end: "2011-05-18T15:01:01Z",
      name: "some updated name",
      slug: "some updated slug",
      start: "2011-05-18T15:01:01Z",
      ticket_price: "456.7",
      ticket_url: "some updated ticket_url"
    }
    @invalid_attrs %{
      description: nil,
      end: nil,
      name: nil,
      slug: nil,
      start: nil,
      ticket_price: nil,
      ticket_url: nil
    }

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Events.create_event()

      event
    end

    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Events.list_events() == [event]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Events.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Events.create_event(@valid_attrs)
      assert event.description == "some description"
      assert event.end == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert event.name == "some name"
      assert event.slug == "some slug"
      assert event.start == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert event.ticket_price == Decimal.new("120.5")
      assert event.ticket_url == "some ticket_url"
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Events.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, %Event{} = event} = Events.update_event(event, @update_attrs)
      assert event.description == "some updated description"
      assert event.end == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert event.name == "some updated name"
      assert event.slug == "some updated slug"
      assert event.start == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert event.ticket_price == Decimal.new("456.7")
      assert event.ticket_url == "some updated ticket_url"
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Events.update_event(event, @invalid_attrs)
      assert event == Events.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Events.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Events.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Events.change_event(event)
    end
  end
end
