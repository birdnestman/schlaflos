defmodule Schlaflos.MembershipsTest do
  use Schlaflos.DataCase

  alias Schlaflos.Memberships

  describe "memberships" do
    alias Schlaflos.Memberships.Membership

    @valid_attrs %{
      active: true,
      batch_id: "some batch_id",
      membership_end: ~D[2010-04-17],
      membership_start: ~D[2010-04-17],
      state: "some state",
      type: "some type"
    }
    @update_attrs %{
      active: false,
      batch_id: "some updated batch_id",
      membership_end: ~D[2011-05-18],
      membership_start: ~D[2011-05-18],
      state: "some updated state",
      type: "some updated type"
    }
    @invalid_attrs %{
      active: nil,
      batch_id: nil,
      membership_end: nil,
      membership_start: nil,
      state: nil,
      type: nil
    }

    def membership_fixture(attrs \\ %{}) do
      {:ok, membership} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Memberships.create_membership()

      membership
    end

    test "list_memberships/0 returns all memberships" do
      membership = membership_fixture()
      assert Memberships.list_memberships() == [membership]
    end

    test "get_membership!/1 returns the membership with given id" do
      membership = membership_fixture()
      assert Memberships.get_membership!(membership.id) == membership
    end

    test "create_membership/1 with valid data creates a membership" do
      assert {:ok, %Membership{} = membership} = Memberships.create_membership(@valid_attrs)
      assert membership.active == true
      assert membership.batch_id == "some batch_id"
      assert membership.membership_end == ~D[2010-04-17]
      assert membership.membership_start == ~D[2010-04-17]
      assert membership.state == "some state"
      assert membership.type == "some type"
    end

    test "create_membership/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Memberships.create_membership(@invalid_attrs)
    end

    test "update_membership/2 with valid data updates the membership" do
      membership = membership_fixture()

      assert {:ok, %Membership{} = membership} =
               Memberships.update_membership(membership, @update_attrs)

      assert membership.active == false
      assert membership.batch_id == "some updated batch_id"
      assert membership.membership_end == ~D[2011-05-18]
      assert membership.membership_start == ~D[2011-05-18]
      assert membership.state == "some updated state"
      assert membership.type == "some updated type"
    end

    test "update_membership/2 with invalid data returns error changeset" do
      membership = membership_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Memberships.update_membership(membership, @invalid_attrs)

      assert membership == Memberships.get_membership!(membership.id)
    end

    test "delete_membership/1 deletes the membership" do
      membership = membership_fixture()
      assert {:ok, %Membership{}} = Memberships.delete_membership(membership)
      assert_raise Ecto.NoResultsError, fn -> Memberships.get_membership!(membership.id) end
    end

    test "change_membership/1 returns a membership changeset" do
      membership = membership_fixture()
      assert %Ecto.Changeset{} = Memberships.change_membership(membership)
    end
  end
end
