defmodule Schlaflos.Factory do
  # with Ecto
  use ExMachina.Ecto, repo: Schlaflos.Repo

  def user_factory do
    %Schlaflos.Accounts.User{
      full_name: "Jane Smith",
      email: sequence(:email, &"email-#{&1}@example.com"),
      role: sequence(:role, ["admin", "user", "other"]),
      hashed_password: Bcrypt.hash_pwd_salt("password"),
      confirmed_at: NaiveDateTime.utc_now(),
      address: sequence(:email, &"Haldiweg Nr. #{&1}"),
      gender: sequence(:gender, ["male", "female", "other"]),
      zip: "8000",
      city: "Zürich",
      birthday: NaiveDateTime.utc_now(),
      member_id: sequence(:member_id, &"S #{&1}")
    }
  end

  def membership_factory do
    %Schlaflos.Memberships.Membership{
      user_id: 1,
      active: true,
      state: "created",
      token: "36c9ef94-b4da-4758-97fc-3882d4961d0c"
    }
  end
end
