defmodule Schlaflos.Repo.Migrations.AddUniqueIndexMemberId do
  use Ecto.Migration

  def change do
    create unique_index(:users, [:member_id])
  end
end
