defmodule Schlaflos.Repo.Migrations.CreateImages do
  use Ecto.Migration

  def change do
    create table(:images) do
      add :name, :string, null: false
      add :type, :string, null: false, size: 10
      add :uuid, :uuid
      add :versions, :map

      timestamps()
    end

    alter table(:artists) do
      remove :image
      add :image_id, :integer
    end

    alter table(:events) do
      remove :image
      add :image_id, :integer
    end
  end
end
