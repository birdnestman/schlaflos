defmodule Schlaflos.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :name, :string
      add :description, :text
      add :start, :utc_datetime
      add :end, :utc_datetime
      add :ticket_price, :decimal
      add :ticket_url, :string
      add :slug, :string
      add :image, :string

      timestamps()
    end

    create unique_index(:events, [:slug])
  end
end
