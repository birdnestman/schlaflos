defmodule Schlaflos.Repo.Migrations.CreateEventArtists do
  use Ecto.Migration

  def change do
    create table(:event_artists) do
      add :event_id, references(:events)
      add :artist_id, references(:artists)
    end

    create unique_index(:event_artists, [:event_id, :artist_id])
  end
end
