defmodule Schlaflos.Repo.Migrations.CreatePages do
  use Ecto.Migration

  def change do
    create table(:pages) do
      add :title, :string
      add :content, :text
      add :slug, :string
      add :meta, :string

      timestamps()
    end
  end
end
