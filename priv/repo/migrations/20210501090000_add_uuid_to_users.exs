defmodule Schlaflos.Repo.Migrations.AddUuidToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :uuid, :uuid
    end
  end
end
