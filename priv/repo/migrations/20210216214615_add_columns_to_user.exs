defmodule Schlaflos.Repo.Migrations.AddColumnsToUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :role, :string, default: "user"
      add :gender, :string
      add :full_name, :string
      add :address, :string
      add :zip, :integer
      add :city, :string
      add :birthday, :date
    end
  end
end
