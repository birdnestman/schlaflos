defmodule Schlaflos.Repo.Migrations.CreateMixes do
  use Ecto.Migration

  def change do
    create table(:mixes) do
      add :name, :string
      add :url, :string
      add :image_id, :integer
      add :artist_id, references(:artists, on_delete: :nothing)

      timestamps()
    end

    create index(:mixes, [:artist_id])
  end
end
