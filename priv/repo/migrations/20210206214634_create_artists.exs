defmodule Schlaflos.Repo.Migrations.CreateArtists do
  use Ecto.Migration

  def change do
    create table(:artists) do
      add :name, :string
      add :website, :string
      add :soundcloud, :string
      add :image, :string
      add :slug, :string
      add :type, :string

      timestamps()
    end

    create unique_index(:artists, [:slug])
  end
end
