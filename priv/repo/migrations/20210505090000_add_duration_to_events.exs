defmodule Schlaflos.Repo.Migrations.AddDurationToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :duration, :integer
      remove :end
    end
  end
end
