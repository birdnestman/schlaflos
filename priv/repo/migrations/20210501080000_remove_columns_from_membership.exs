defmodule Schlaflos.Repo.Migrations.RemoveColumnsFromMembershipd do
  use Ecto.Migration

  def change do
    alter table(:memberships) do
      remove :type
      remove :batch_id
      remove :membership_start
      remove :membership_end
    end
  end
end
