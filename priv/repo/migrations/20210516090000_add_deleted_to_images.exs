defmodule Schlaflos.Repo.Migrations.AddDeletedToImages do
  use Ecto.Migration

  def change do
    alter table(:images) do
      add :deleted, :boolean, default: false
    end
  end
end
