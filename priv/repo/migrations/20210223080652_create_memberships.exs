defmodule Schlaflos.Repo.Migrations.CreateMemberships do
  use Ecto.Migration

  def change do
    create table(:memberships) do
      add :user_id, references(:users, on_delete: :nothing)
      add :active, :boolean, default: false, null: false
      add :type, :string
      add :batch_id, :string
      add :membership_start, :date
      add :membership_end, :date
      add :state, :string
      add :token, :string

      timestamps()
    end

    create index(:memberships, [:user_id])
  end
end
