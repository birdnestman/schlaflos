defmodule Schlaflos.Repo.Migrations.AddGenreToArtists do
  use Ecto.Migration

  def change do
    alter table(:artists) do
      add :genre, :string
    end
  end
end
