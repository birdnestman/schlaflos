defmodule Schlaflos.Repo.Migrations.AddFavoriteFlagToArtists do
  use Ecto.Migration

  def change do
    alter table(:artists) do
      add :favorite, :boolean
    end
  end
end
