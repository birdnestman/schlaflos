defmodule Schlaflos.Repo.Migrations.AddMemberRoleToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :member_role, :string, default: "member"
      add :member_id, :string
    end
  end
end
