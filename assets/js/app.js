// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import "phoenix_html"
import {Socket} from "phoenix"
import NProgress from "nprogress"
import {LiveSocket} from "phoenix_live_view"
import EasyMDE from "easymde"

import 'alpinejs'


let Uploaders = {}
Uploaders.S3 = function(entries, onViewError){
  entries.forEach(entry => {
    let formData = new FormData()
    let {url, fields} = entry.meta
    Object.entries(fields).forEach(([key, val]) => formData.append(key, val))

    formData.append("file", entry.file)

    let xhr = new XMLHttpRequest()
    onViewError(() => xhr.abort())
    xhr.onload = () => xhr.status === 204 ? entry.progress(100) : entry.error()
    xhr.onerror = () => entry.error()
    xhr.upload.addEventListener("progress", (event) => {
      if(event.lengthComputable){
        let percent =  Math.round((event.loaded / event.total) * 100)
        if(percent < 100) { entry.progress(percent)}
      }
    })
    xhr.open("POST", url, true)
    xhr.send(formData)
  })
}

let Hooks = {}
Hooks.TextEditor = {
  mounted() {
    var easyMDE = new EasyMDE({element: this.el});
  },
  updated() {
    var easyMDE = new EasyMDE({element: this.el});
  }
}

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket('/live', Socket, {
  uploaders: Uploaders,
  dom: {
    // make LiveView work nicely with alpinejs
    onBeforeElUpdated(from, to) {
      if (from.__x) {
        window.Alpine.clone(from.__x, to);
      }
    },
  },
  params: {
    _csrf_token: csrfToken,
  },
  hooks: Hooks,
});

// Show progress bar on live navigation and form submits
window.addEventListener("phx:page-loading-start", info => NProgress.start())
window.addEventListener("phx:page-loading-stop", info => NProgress.done())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket


// Bulma Navbar Burger
document.addEventListener('DOMContentLoaded', () => {

  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-hidden');

      });
    });
  }
});

document.addEventListener('snipcart.ready', function() {
  Snipcart.api.session.setLanguage('de', {
    "payment": {
      "methods": {
        "deferred_payment": "Vorauskasse (Momentan nur Vorauskasse möglich)"
      },
      "form": {
        "deferred_payment_title": "Vorauskasse",
        "deferred_payment_instructions": "Überweise den Betrag im Voraus an folgendes Konto: <br><br>IBAN: CH37 8096 5000 0122 8080 9 <br> Verein Mono.Ton, Tellistrasse 118, 5000 Aarau<br><br>Sobald wir den Betrag erhalten haben, schicken wir dir deine Bestellung zu."
      }
    }
  });
});