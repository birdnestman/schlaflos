# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :schlaflos,
  ecto_repos: [Schlaflos.Repo],
  snipcart_api_key: System.get_env("SNIPCART_API_KEY")

# Configures the endpoint
config :schlaflos, SchlaflosWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "JRDa2KJSb9fmn777v1tmBS65gL0LfTfho0EESMh/VtyuTYKnCtF4QdcSA47M514E",
  render_errors: [view: SchlaflosWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Schlaflos.PubSub,
  live_view: [signing_salt: "teAEl4I2"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :timex, :default_locale, "de"

config :ex_aws,
  access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, :instance_role],
  secret_access_key: [{:system, "AWS_SECRET_ACCESS_KEY"}, :instance_role],
  bucket_name: [{:system, "BUCKET_NAME"}, :instance_role],
  s3: [
    scheme: "https://",
    host: "schlaflos.s3.amazonaws.com",
    region: "eu-central-1"
  ]

config :schlaflos,
  thumbnails: %{
    small: "w-500",
    medium: "w-800",
    large: "w-1920"
  }

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
