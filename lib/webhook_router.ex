defmodule SchlaflosWeb.WebhookRouter do
  use SchlaflosWeb, :router

  scope "/webhook", SchlaflosWeb do
    post("/order.completed", WebhookController, :order_completed)
    post("/order.status.changed", WebhookController, :order_status_changed)
    post("/error", WebhookController, :error)
  end
end
