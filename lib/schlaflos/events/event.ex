defmodule Schlaflos.Events.Event do
  use Ecto.Schema
  import Ecto.Changeset

  schema "events" do
    field :description, :string
    field :name, :string
    field :slug, :string
    field :start, :naive_datetime
    field :duration, :integer
    field :ticket_price, :decimal
    field :ticket_url, :string
    belongs_to :image, Schlaflos.Images.Image

    many_to_many :artists, Schlaflos.Artists.Artist,
      join_through: "event_artists",
      on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(event, attrs) do
    attrs = Map.merge(attrs, slug_map(attrs))

    event
    |> cast(attrs, [:name, :description, :start, :duration, :ticket_price, :ticket_url, :slug])
    |> validate_required([
      :name,
      :description,
      :slug,
      :start,
      :duration
    ])
    |> unique_constraint(:slug)
  end

  @doc false
  def changeset_artists(event, attrs, artists) do
    event
    |> changeset(attrs)
    |> put_assoc(:artists, artists)
  end

  def changeset_image(event, image) do
    event
    |> change()
    |> put_assoc(:image, image)
  end

  defp slug_map(%{"name" => name}) do
    slug =
      name
      |> String.downcase()
      |> String.replace(" ", "-")
      |> String.replace(".", "-")
      |> String.replace("/", "")
      |> String.replace("\\", "")
      |> String.replace("&", "und")
      |> String.replace("'", "")

    %{"slug" => slug}
  end

  defp slug_map(_params) do
    %{}
  end

  defimpl Phoenix.Param, for: Schlaflos.Events.Event do
    def to_param(%{slug: slug}) do
      "#{slug}"
    end
  end
end
