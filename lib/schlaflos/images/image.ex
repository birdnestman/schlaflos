defmodule Schlaflos.Images.Image do
  use Ecto.Schema
  import Ecto.Changeset

  schema "images" do
    field :type, :string
    field :name, :string
    field :uuid, Ecto.UUID
    field :versions, :map
    field :deleted, :boolean

    has_one :artist, Schlaflos.Artists.Artist
    has_one :event, Schlaflos.Events.Event

    timestamps()
  end

  @doc false
  def changeset(image, attrs) do
    image
    |> cast(attrs, [:name, :type, :uuid, :deleted])
    |> validate_required([:name, :type, :uuid])
  end

  def changeset_versions(image, attrs) do
    image
    |> cast(attrs, [:versions])
    |> validate_required([:versions])
  end
end
