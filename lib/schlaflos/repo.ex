defmodule Schlaflos.Repo do
  use Ecto.Repo,
    otp_app: :schlaflos,
    adapter: Ecto.Adapters.Postgres
end
