defmodule Schlaflos.Events do
  @moduledoc """
  The Events context.
  """

  import Ecto.Query, warn: false

  alias Schlaflos.Repo

  alias Schlaflos.Events.Event
  alias Schlaflos.Artists.Artist

  @doc """
  Returns the list of upcoming events.

  ## Examples

      iex> list_events()
      [%Event{}, ...]

  """
  def list_upcoming_events do
    date = Date.utc_today()

    Event
    |> where([e], fragment("?::date", e.start) >= ^date)
    |> order_by(:start)
    |> Repo.all()
    |> Repo.preload([:artists, :image])
  end

  @doc """
  Returns the list of events.

  ## Examples

      iex> list_events()
      [%Event{}, ...]

  """
  def list_events do
    Event
    |> order_by(desc: :start)
    |> Repo.all()
    |> Repo.preload([:artists, :image])
  end

  @doc """
  Gets a single event.

  Raises `Ecto.NoResultsError` if the Event does not exist.

  ## Examples

      iex> get_event!(123)
      %Event{}

      iex> get_event!(456)
      ** (Ecto.NoResultsError)

  """
  def get_event!(id) do
    case Repo.get_by(Event, slug: id) do
      nil -> Repo.get!(Event, id)
      result -> result
    end
  end

  @doc """
  Creates a event.

  ## Examples

      iex> create_event(%{field: value})
      {:ok, %Event{}}

      iex> create_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event(%Event{} = event, attrs \\ %{}, after_save \\ &{:ok, &1}, artists) do
    event
    |> Event.changeset_artists(attrs, artists)
    |> Repo.insert()
    |> after_save(after_save)
  end

  defp after_save({:ok, event}, func) do
    {:ok, _event} = func.(event)
  end

  defp after_save(error, _func), do: error

  @doc """
  Updates a event.

  ## Examples

      iex> update_event(event, %{field: new_value})
      {:ok, %Event{}}

      iex> update_event(event, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_event(%Event{} = event, attrs, after_save \\ &{:ok, &1}, artists) do
    event
    |> Repo.preload(:artists)
    |> Event.changeset_artists(attrs, artists)
    |> Repo.update()
    |> after_save(after_save)
  end

  @doc """
  Deletes a event.

  ## Examples

      iex> delete_event(event)
      {:ok, %Event{}}

      iex> delete_event(event)
      {:error, %Ecto.Changeset{}}

  """
  def delete_event(%Event{} = event) do
    Repo.delete(event)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking event changes.

  ## Examples

      iex> change_event(event)
      %Ecto.Changeset{data: %Event{}}

  """
  def change_event(%Event{} = event, attrs \\ %{}) do
    Event.changeset(event, attrs)
  end

  def get_artists(%Event{id: nil} = _event), do: []

  def get_artists(%Event{id: event_id} = _event) do
    (a in Artist)
    |> from()
    |> join(:left, [a], ea in "event_artists", on: a.id == ea.artist_id)
    |> where([a, ea], ea.event_id == ^event_id)
    |> order_by([a, ea], asc: a.name)
    |> Repo.all()
    |> Repo.preload(:image)
  end

  def get_genres(artists) do
    artists
    |> Enum.reduce([], fn artist, acc ->
      case artist.genre do
        "-" -> acc
        _ -> acc ++ [artist.genre]
      end
    end)
    |> Enum.uniq()
  end

  def add_image(artist, image) do
    artist
    |> Repo.preload(:image)
    |> Artist.changeset_image(image)
    |> Repo.update!()
  end
end
