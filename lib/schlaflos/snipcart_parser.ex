defmodule Schlaflos.SnipcartParser do
  def parse_user(params) do
    %{
      full_name: get_in(params, ["content", "user", "billingAddress", "fullName"]),
      address: get_in(params, ["content", "user", "billingAddress", "address1"]),
      zip: String.to_integer(get_in(params, ["content", "user", "billingAddress", "postalCode"])),
      city: get_in(params, ["content", "user", "billingAddress", "city"]),
      email: get_in(params, ["content", "email"])
    }
  end

  def parse_items(params) do
    Enum.reduce(get_in(params, ["content", "items"]), [], fn item, acc ->
      map = %{
        id: item["id"],
        price: item["price"],
        name: item["name"]
      }

      acc ++ [map]
    end)
  end

  def parse_token(params) do
    get_in(params, ["content", "token"])
  end

  def has_membership?(%{"content" => %{"items" => items}}) do
    items
    |> Enum.reduce_while(false, fn item, _acc ->
      if item["name"] =~ "mitgliedschaft", do: {:halt, true}, else: {:cont, true}
    end)
  end

  def parse_target_state(params) do
    get_in(params, ["to"])
  end

  def get_email(params), do: get_in(params, ["content", "email"])

  def get_payment_status(params) do
    get_in(params, ["content", "paymentStatus"])
  end
end
