defmodule Schlaflos.Artists.Mix do
  use Ecto.Schema
  import Ecto.Changeset

  schema "mixes" do
    field :name, :string
    field :url, :string
    belongs_to :image, Schlaflos.Images.Image, on_replace: :update
    belongs_to :artist, Schlaflos.Artists.Artist

    timestamps()
  end

  @doc false
  def changeset(mix, attrs) do
    mix
    |> cast(attrs, [:artist_id, :name, :url])
    |> validate_required([:artist_id, :name, :url])
  end
end
