defmodule Schlaflos.Artists.Artist do
  use Ecto.Schema
  import Ecto.Changeset

  schema "artists" do
    field :name, :string
    field :soundcloud, :string
    field :website, :string
    field :type, :string
    field :slug, :string
    field :favorite, :boolean
    field :genre, :string
    belongs_to :image, Schlaflos.Images.Image, on_replace: :update
    has_many :mixes, Schlaflos.Artists.Mix

    many_to_many :events, Schlaflos.Events.Event,
      join_through: "event_artists",
      on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(artist, attrs) do
    attrs = Map.merge(attrs, slug_map(attrs))

    artist
    |> cast(attrs, [:name, :website, :soundcloud, :type, :slug, :favorite, :genre])
    |> validate_required([:name, :type, :slug, :genre, :favorite])
    |> unique_constraint(:slug)
  end

  def changeset_image(artist, image) do
    artist
    |> cast(%{image_id: image.id}, [:image_id])
  end

  def slug_map(%{"name" => name}) do
    slug =
      name
      |> String.downcase()
      |> String.replace(" ", "-")
      |> String.replace(".", "-")
      |> String.replace("/", "")
      |> String.replace("\\", "")
      |> String.replace("&", "und")
      |> String.replace("'", "")

    %{"slug" => slug}
  end

  def slug_map(_params) do
    %{}
  end

  defimpl Phoenix.Param, for: Schlaflos.Artists.Artist do
    def to_param(%{slug: slug}) do
      "#{slug}"
    end
  end
end
