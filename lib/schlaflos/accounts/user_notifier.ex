defmodule Schlaflos.Accounts.UserNotifier do
  import Bamboo.Email
  alias Schlaflos.Mailer

  @from_address "noreply@schlaflos-club.ch"

  defp deliver(to, subject, text_body, html_body) do
    email =
      new_email(
        to: to,
        from: @from_address,
        subject: subject,
        text_body: text_body,
        html_body: html_body
      )
      |> Mailer.deliver_now()

    {:ok, email}
  end

  @doc """
  Deliver instructions to confirm account.
  """
  def deliver_confirmation_instructions(user, url) do
    text_body = """

    ==============================

    Hallo #{user.firstname},

    Du kannst deinen Account bestätigen indem du auf folgenden Link klickst:

    #{url}

    Falls du keinen Account erstellt hast kannst du dieses E-Mail ignorieren.

    ==============================
    """

    html_body = """
    Hallo #{user.full_name},<br/></br/>
    Du kannst deinen Account bestätigen indem du auf folgenden Link klickst:<br/></br/>
    <a href="#{url}" target="_blank">#{url}</a><br/></br/>
    Falls du keinen Account bei uns erstellt hast, kannst du dieses E-Mail ignorieren.
    """

    deliver(user.email, "Bitte bestätige deinen Account", text_body, html_body)
  end

  @doc """
  Deliver instructions to reset password account.
  """
  def deliver_reset_password_instructions(user, url) do
    text_body = """

    ==============================

    Hallo #{user.full_name},

    Du kannst dein Passwort zurücksetzen indem du auf folgenden Link klickst:

    #{url}

    Falls du diese Änderung nicht angefordert hast, kannst du dieses E-Mail ignorieren.

    ==============================
    """

    html_body = """
    Hallo #{user.full_name},<br/></br/>
    Du kannst dein Passwort zurücksetzen, indem du auf folgenden Link klickst:<br/></br/>
    <a href="#{url}" target="_blank">#{url}</a><br/></br/>
    Falls du diese Änderung nicht angefordert hast, kannst du dieses E-Mail ignorieren.
    """

    deliver(user.email, "Dein Passwort zurücksetzen", text_body, html_body)
  end

  @doc """
  Deliver instructions to update your e-mail.
  """
  def deliver_update_email_instructions(user, url) do
    text_body = """

    ==============================

    Hallo #{user.full_name},

    Du kannst deine E-Mail-Adresse ändern, indem du auf folgenden Link klickst:

    #{url}

    Falls du diese Änderung nicht angefordert hast, kannst du dieses E-Mail ignorieren.

    ==============================
    """

    html_body = """
    Hallo #{user.full_name},<br/></br/>
    Du kannst deine E-Mail-Adresse ändern, indem du auf folgenden Link klickst:<br/></br/>
    <a href="#{url}" target="_blank">#{url}</a><br/></br/>
    Falls du diese Änderung nicht angefordert hast, kannst du dieses E-Mail ignorieren.
    """

    deliver(user.email, "Deine E-Mail-Adresse ändern", text_body, html_body)
  end
end
