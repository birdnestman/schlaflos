defmodule Schlaflos.Artists do
  @moduledoc """
  The Artists context.
  """

  import Ecto.Query, warn: false
  alias Schlaflos.Repo

  alias Schlaflos.Images
  alias Schlaflos.Artists.Artist
  alias Schlaflos.Artists.Mix
  alias Schlaflos.Events.Event

  @doc """
  Returns the list of artists.

  ## Examples

      iex> list_artists()
      [%Artist{}, ...]

  """
  def list_artists do
    Artist
    |> order_by(asc: :name)
    |> Repo.all()
    |> Repo.preload(:image)
  end

  def list_artists_for_select do
    (a in Artist)
    |> from()
    |> select([a], {a.name, a.id})
    |> order_by(asc: :name)
    |> Repo.all()
  end

  def list_favorite_artists do
    Artist
    |> where([a], a.favorite == true)
    |> order_by(asc: :name)
    |> Repo.all()
    |> Repo.preload(:image)
  end

  def list_artists_by_favorits do
    Artist
    |> order_by(desc: :favorite, asc: :name)
    |> Repo.all()
    |> Repo.preload(:image)
  end

  def find_artists(string) do
    Artist
    |> where([a], like(fragment("lower(?)", a.name), ^"%#{string}%"))
    |> order_by(asc: :name)
    |> Repo.all()
  end

  @doc """
  Gets a single artist.

  Raises `Ecto.NoResultsError` if the Artist does not exist.

  ## Examples

      iex> get_artist!(123)
      %Artist{}

      iex> get_artist!(456)
      ** (Ecto.NoResultsError)

  """
  def get_artist!(id) do
    case Repo.get_by(Artist, slug: id) do
      nil -> Repo.get!(Artist, id) |> Repo.preload(:image)
      result -> result |> Repo.preload(:image)
    end
  end

  @doc """
  Creates a artist.

  ## Examples

      iex> create_artist(%{field: value})
      {:ok, %Artist{}}

      iex> create_artist(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_artist(%Artist{} = artist, attrs \\ %{}, after_save \\ &{:ok, &1}) do
    artist
    |> Artist.changeset(attrs)
    |> Repo.insert()
    |> after_save(after_save)
  end

  defp after_save({:ok, artist}, func) do
    {:ok, _artist} = func.(artist)
  end

  defp after_save(error, _func), do: error

  @doc """
  Updates a artist.

  ## Examples

      iex> update_artist(artist, %{field: new_value})
      {:ok, %Artist{}}

      iex> update_artist(artist, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_artist(%Artist{} = artist, attrs, after_save \\ &{:ok, &1}) do
    artist
    |> Artist.changeset(attrs)
    |> Repo.update()
    |> after_save(after_save)
  end

  @doc """
  Deletes a artist.

  ## Examples

      iex> delete_artist(artist)
      {:ok, %Artist{}}

      iex> delete_artist(artist)
      {:error, %Ecto.Changeset{}}

  """
  def delete_artist(%Artist{} = artist) do
    Repo.delete(artist)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking artist changes.

  ## Examples

      iex> change_artist(artist)
      %Ecto.Changeset{data: %Artist{}}

  """
  def change_artist(%Artist{} = artist, attrs \\ %{}) do
    Artist.changeset(artist, attrs)
  end

  def get_events(%Artist{id: artist_id} = _artist) do
    (e in Event)
    |> from()
    |> join(:left, [e], ea in "event_artists", on: e.id == ea.event_id)
    |> where([e, ea], ea.artist_id == ^artist_id)
    |> Repo.all()
    |> Repo.preload([:artists, :image])
  end

  def add_image(artist, image) do
    artist
    |> Repo.preload(:image)
    |> remove_image()
    |> Artist.changeset_image(image)
    |> Repo.update!()
  end

  def remove_image(%Schlaflos.Artists.Artist{image: nil} = artist), do: artist

  def remove_image(%Schlaflos.Artists.Artist{image: image} = artist) do
    Images.update_image(image, %{deleted: true})
    artist
  end

  def unset_image(artist) do
    artist
    |> remove_image()
    |> Artist.changeset_image(%{id: nil})
    |> Repo.update!()
  end

  def list_mixes do
    Mix
    |> order_by(asc: :name)
    |> Repo.all()
    |> Repo.preload([:artist, :image])
  end

  def get_mix!(id), do: Repo.get!(Mix, id)

  def change_mix(%Mix{} = mix, attrs \\ %{}) do
    Mix.changeset(mix, attrs)
  end

  def create_mix(%Mix{} = event, attrs \\ %{}, after_save \\ &{:ok, &1}) do
    event
    |> Mix.changeset(attrs)
    |> Repo.insert()
    |> after_save(after_save)
  end

  def update_mix(%Mix{} = event, attrs, after_save \\ &{:ok, &1}) do
    event
    |> Mix.changeset(attrs)
    |> Repo.update()
    |> after_save(after_save)
  end

  def delete_mix(%Mix{} = mix) do
    Repo.delete(mix)
  end
end
