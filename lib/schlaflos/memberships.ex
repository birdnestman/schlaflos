defmodule Schlaflos.Memberships do
  @moduledoc """
  The Memberships context.
  """

  import Ecto.Query, warn: false

  use Timex

  alias Schlaflos.Repo
  alias Schlaflos.Memberships.Membership

  @doc """
  Returns the list of memberships.

  ## Examples

      iex> list_memberships()
      [%Membership{}, ...]

  """
  def list_memberships do
    Membership
    |> Repo.all()
    |> Repo.preload(:user)
  end

  @doc """
  Gets a single membership.

  Raises `Ecto.NoResultsError` if the Membership does not exist.

  ## Examples

      iex> get_membership!(123)
      %Membership{}

      iex> get_membership!(456)
      ** (Ecto.NoResultsError)

  """
  def get_membership!(id), do: Repo.get!(Membership, id)

  def get_membership_by_token!(token), do: Repo.get_by!(Membership, token: token)

  @doc """
  Creates a membership.

  ## Examples

      iex> create_membership(%{field: value})
      {:ok, %Membership{}}

      iex> create_membership(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_membership(attrs \\ %{}) do
    %Membership{}
    |> Membership.changeset_silent(attrs)
    |> Repo.insert()
  end

  def create_silent_membership(%{id: user_id} = _user, token) do
    attrs = %{user_id: user_id, active: true, type: "member", state: "paid", token: token}

    %Membership{}
    |> Membership.changeset_silent(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a membership.

  ## Examples

      iex> update_membership(membership, %{field: new_value})
      {:ok, %Membership{}}

      iex> update_membership(membership, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_membership(%Membership{} = membership, attrs) do
    membership
    |> Membership.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a membership.

  ## Examples

      iex> delete_membership(membership)
      {:ok, %Membership{}}

      iex> delete_membership(membership)
      {:error, %Ecto.Changeset{}}

  """
  def delete_membership(%Membership{} = membership) do
    Repo.delete(membership)
  end

  def change_membership_state(token, params) do
    token
    |> get_membership_by_token!
    |> Membership.changeset_state(params)
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking membership changes.

  ## Examples

      iex> change_membership(membership)
      %Ecto.Changeset{data: %Membership{}}

  """
  def change_membership(%Membership{} = membership, attrs \\ %{}) do
    Membership.changeset_silent(membership, attrs)
  end

  def get_state(%{memberships: memberships}) do
    memberships
    |> Enum.reduce(%{}, fn x, _acc ->
      if x.active == true do
        %{
          active: true,
          start: x.inserted_at,
          end: Timex.shift(x.inserted_at, years: 1)
        }
      end
    end)
  end

  def get_membership_enddate(user) do
    user
    |> Repo.preload(:memberships)
    |> Schlaflos.Memberships.get_state()
    |> Map.get(:end)
    |> case do
      nil -> "_"
      date -> Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{YYYY}-{M}-{D}")
    end
  end
end
