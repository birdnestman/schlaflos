defmodule Schlaflos.Pages.Page do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Phoenix.Param, key: :slug}
  schema "pages" do
    field :content, :string
    field :meta, :string
    field :slug, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(page, attrs) do
    attrs = Map.merge(attrs, slug_map(attrs))

    page
    |> cast(attrs, [:title, :content, :slug, :meta])
    |> validate_required([:title, :content])
  end

  defp slug_map(%{"title" => title}) do
    slug = String.downcase(title) |> String.replace(" ", "-")
    %{"slug" => slug}
  end

  defp slug_map(_params) do
    %{}
  end

  defimpl Phoenix.Param, for: Teacher.Post do
    def to_param(%{slug: slug}) do
      "#{slug}"
    end
  end
end
