defmodule Schlaflos.Images do
  @moduledoc """
  The Images context.
  """

  import Ecto.Query, warn: false

  alias Schlaflos.Repo
  alias Schlaflos.Images.{Image}

  def get_images() do
    Image
    |> Repo.all()
  end

  def get_image!(id), do: Repo.get!(Image, id)

  def update_image(%Image{} = image, attrs) do
    image
    |> Image.changeset(attrs)
    |> Repo.update()
  end

  def create_image(%Image{} = image, attrs \\ %{}) do
    image
    |> Image.changeset(attrs)
    |> Repo.insert()
  end

  def update_versions(%Image{} = image, attrs \\ %{}) do
    image
    |> Image.changeset_versions(attrs)
    |> Repo.update()
  end

  def get(image) do
    default = List.first(Application.get_env(:schlaflos, :thumbnails, []))
    get(image, default)
  end

  def get(nil, version), do: get(get_anonymous(), version)
  def get(%Ecto.Association.NotLoaded{}, _), do: ""
  # def get(image, version) when is_atom(version), do: get(image, Atom.to_string(version))

  def get(image, version) when is_map(image) do
    size = Map.get(Application.get_env(:schlaflos, :thumbnails), version)

    "https://ik.imagekit.io/schlaflos/#{image.uuid}/original.#{image.type}?tr=#{size}"
  end

  def get_url(image) do
    "https://schlaflos.s3.amazonaws.com/#{Mix.env()}/" <> image.uuid <> "/original." <> image.type
  end

  def get_anonymous() do
    Image
    |> where([i], i.name == "anonymous")
    |> limit(1)
    |> Repo.one()
  end
end
