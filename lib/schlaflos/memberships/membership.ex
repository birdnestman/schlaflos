defmodule Schlaflos.Memberships.Membership do
  use Ecto.Schema

  import Ecto.Changeset

  alias Schlaflos.Accounts.User

  schema "memberships" do
    field :active, :boolean, default: false
    field :state, :string
    field :token, :string
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset_silent(membership, attrs) do
    membership
    |> cast(attrs, [:user_id, :active, :state, :token])
    |> validate_required([:user_id, :active, :state, :token])
  end

  @doc false
  def changeset_state(membership, attrs) do
    membership
    |> cast(attrs, [:active, :state])
    |> validate_required([:active, :state])
  end
end
