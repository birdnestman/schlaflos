defmodule SchlaflosWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use SchlaflosWeb, :controller
      use SchlaflosWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: SchlaflosWeb

      import Plug.Conn
      import SchlaflosWeb.Gettext
      alias SchlaflosWeb.Router.Helpers, as: Routes
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/schlaflos_web/templates",
        namespace: SchlaflosWeb

      # Import convenience functions from controllers
      import Phoenix.Controller,
        only: [get_flash: 1, get_flash: 2, view_module: 1, view_template: 1]

      # Include shared imports and aliases for views
      unquote(view_helpers())
    end
  end

  def live_view do
    quote do
      use Phoenix.LiveView,
        layout: {SchlaflosWeb.LayoutView, "live.html"}

      unquote(view_helpers())
    end
  end

  def live_view_admin do
    quote do
      use Phoenix.LiveView,
        layout: {SchlaflosWeb.LayoutView, "live_admin.html"}

      unquote(view_helpers())
    end
  end

  def live_view_infoboard do
    quote do
      use Phoenix.LiveView,
        layout: {SchlaflosWeb.LayoutView, "live_infoboard.html"}

      unquote(view_helpers())
    end
  end

  def live_component do
    quote do
      use Phoenix.LiveComponent

      unquote(view_helpers())
    end
  end

  def router do
    quote do
      use Phoenix.Router

      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import SchlaflosWeb.Gettext
    end
  end

  defp view_helpers do
    quote do
      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      # Import LiveView helpers (live_render, live_component, live_patch, etc)
      import Phoenix.LiveView.Helpers
      import SchlaflosWeb.LiveHelpers

      # Import basic rendering functionality (render, render_layout, etc)
      import Phoenix.View

      import SchlaflosWeb.InputHelpers
      import SchlaflosWeb.ErrorHelpers
      import SchlaflosWeb.Gettext
      alias SchlaflosWeb.Router.Helpers, as: Routes
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
