defmodule SchlaflosWeb.Infoboard.DrinksLive.Index do
  use SchlaflosWeb, :live_view_infoboard

  use Timex

  alias Schlaflos.Events

  @impl true
  @spec mount(any, any, Phoenix.LiveView.Socket.t()) :: {:ok, Phoenix.LiveView.Socket.t()}
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :events, list_events())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Artists")
    |> assign(:artist, nil)
  end

  defp list_events do
    Events.list_upcoming_events()
  end

  def print_from_to(start, _duration) do
    from =
      start
      |> Timezone.convert("Europe/Zurich")
      |> Timex.format!("{D}. {Mshort} {YYYY}")

    "#{from}"
  end

  def get_string_artist(artists) do
    artists
    |> Enum.reduce([], fn artist, acc ->
      acc ++ [artist.name]
    end)
    |> Enum.join(", ")
  end

  def get_genres(artists) do
    artists
    |> Enum.reduce([], fn artist, acc ->
      acc ++ [artist.genre]
    end)
    |> Enum.uniq()
    |> Enum.filter(fn value -> value != "-" end)
  end
end
