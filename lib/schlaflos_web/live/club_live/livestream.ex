defmodule SchlaflosWeb.ClubLive.Livestream do
  use SchlaflosWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    socket =
      socket
      |> page_settings("Frische Musik aus dem Schlafzimmer")

    {:ok, socket}
  end

  defp page_settings(
         socket,
         page_title,
         header_big \\ false,
         header_background \\ "/images/header-image-mainfloor-2.jpg"
       ) do
    socket
    |> assign(
      query: "",
      page_title: page_title,
      header_big: header_big,
      header_background: header_background
    )
  end
end
