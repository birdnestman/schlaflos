defmodule SchlaflosWeb.ClubLive.Shop do
  use SchlaflosWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    title = "Schlaflos Merchandise"

    meta_attrs = [
      %{name: "title", content: title},
      %{
        name: "description",
        content:
          "Bestell jetzt Schlaflos Merchandise von deinem Lieblingsclub! Schau jetzt vorbei und gönn dir was neues!"
      },
      %{property: "og:title", content: "Schlaflos - #{title}"},
      %{
        property: "og:image",
        content: SchlaflosWeb.Router.Helpers.static_url(socket, "/images/schlaflos-seit-2014.jpg")
      }
    ]

    socket =
      socket
      |> assign(meta_attrs: meta_attrs)
      |> page_settings(title)

    {:ok, socket}
  end

  defp page_settings(
         socket,
         page_title,
         header_big \\ false,
         header_background \\ "/images/header-image-mainfloor-2.jpg"
       ) do
    socket
    |> assign(
      query: "",
      page_title: page_title,
      header_big: header_big,
      header_background: header_background
    )
  end
end
