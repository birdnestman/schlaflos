<div class="container">
  <div class="columns">
    <div class="column">
      <div class="ls-section-content">
        <p class="subtitle">Elektronische Musik in Aarau hat einen Namen und dieser Name lautet Schlaflos.</p>
        <p class="content has-text-grey has-text-weight-light is-size-5">
          Seit seiner Gründung hat die Macherschaft des Clubs, der Musik- und Kulturverein Mono.Ton Events, eine musikalische Gangart etabliert, die den unterschiedlichsten Bedürfnissen gerecht wird. Ein Facettenreichtum, der im Schlaflos längst zum programmlichen Inventar gehört. Insgesamt können sich im Schlaflos circa 300 Gäste tummeln. Der Club ist in zwei Stockwerke gegliedert: Unten befindet sich der Mainfloor und oben die Lounge, auch „Stube“ genannt.
        </p>
        <p class="content has-text-grey has-text-weight-light is-size-5">
          Das Schlaflos versteht sich als Club um der Musik willen – das Gewinnstreben muss hier hintenan stehen. Aus einem einfachen Grund: Im Kanton Aargau ist es nicht gerade einfach einen Club mit genügenden Öffnungszeiten und nach international gültigen Qualitätsmassstäben zu leiten und wir wollen hier den Beweis führen, dass es eben doch möglich ist. Wir beschäftigen eigene Licht- und Laser-Fachleute, Videokünstler und Soundtechniker und auch alle anderen Schlaflos-Crewmitglieder gehen ihrer Tätigkeit mit grösster Leidenschaft nach und machen sich mit Freude an der Sache ans Werk. Das merkt man diesem Club auch an: Hier ist die Crew eine grosse Familie und das sorgt für einen ureigenen Groove. Dazu kommt eine erstklassige Programmierung, die immer wieder mal grosse Marken (auch ausländische, wie zum Beipiel den Berliner Ritter Butzke) und international dekorierte Clubmusiker nach Aarau bringt.
        </p>
        <p class="content has-text-grey has-text-weight-light is-size-5">
          All dies zusammen hat dafür gesorgt, dass das Schlaflos längst zur überregional beliebten Anlaufstelle für Clubberinnen und Clubber avanciert ist.
        </p>
      </div>
    </div>
  </div>
</div>

<div class="columns mt-5">
  <div class="column is-6 is-offset-3">
    <div id="divider"></div>
  </div>
</div>

<section class="section">
  <div class="container">
    <div class="is-max-w-lg mx-auto has-text-centered">
      <h2 class="mt-5 title">Werde Mitglied im Kulturverein Mono.Ton und unterstütze das Schlaflos!</h2>
      <div class="mt-5"><a class="button is-primary mr-2 is-large" href="/mitglied-werden">Jetzt Migtlied werden!</a></div>
    </div>
  </div>
</section>


<section class="section ls-background-light">
  <div class="container">
    <div class="columns">
      <div class="column is-8 is-offset-2">
        <div class="ls-section-content has-text-centered"><h2 class="title has-text-dark title-deco-center">Bekannte Gesichter im Schlaflos</h2>
          <p class="content has-text-grey has-text-weight-light is-size-5">Eine Auswahl von Artisten welche bereits bei uns performt haben. Die komplette Liste findest du <a href="/artists">hier.</a></p>
        </div>
      </div>
    </div>
    <div class="columns is-multiline mosaic">

      <%= for artist <- @artists do %>
      <div class="column is-6 is-3-desktop has-text-centered mosaic-image" id="artist-<%= artist.id %>">
        <a href="/artists/<%= artist.slug %>">
          <img class="mb-4 is-cover-256x256 is-radius"
               src="<%= Schlaflos.Images.get(artist.image, :medium) %>"
               alt="Bild von <%= artist.name %>">
          <h3 class="title is-size-5"><%= artist.name %></h3>
          <p class="subtitle mb-4 has-text-info">
            <span class="tag is-primary is-light">#<%= String.downcase(artist.type) %></span>
            <%= if artist.genre != "-" do %>
              <span class="tag is-primary is-light">#<%= artist.genre |> String.downcase() |> String.replace(" ", "_") %></span>
            <% end %>
          </p>
        </a>
      </div>
      <% end %>
    </div>
  </div>
</section>
