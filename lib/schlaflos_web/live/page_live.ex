defmodule SchlaflosWeb.PageLive do
  use SchlaflosWeb, :live_view
  use Timex

  alias Schlaflos.{Artists, Events}

  @impl true
  def mount(_params, _session, socket) do
    title = "Alle Veranstaltungen und Events"
    description = "Elektronische Musik in Aarau hat einen Namen und dieser Name lautet Schlaflos."

    meta_attrs = [
      %{name: "title", content: title},
      %{
        name: "description",
        content: description
      },
      %{property: "og:title", content: "Schlaflos - #{title}"},
      %{
        property: "og:image",
        content: SchlaflosWeb.Router.Helpers.static_url(socket, "/images/schlaflos-seit-2014.jpg")
      },
      %{
        property: "og:description",
        content: SchlaflosWeb.Router.Helpers.static_url(socket, "/images/schlaflos-seit-2014.jpg")
      }
    ]

    events = Events.list_upcoming_events()
    mixes = Artists.list_mixes()

    socket =
      socket
      |> assign(:meta_attrs, meta_attrs)
      |> assign(query: "", events: events, mixes: mixes)
      |> page_settings(title, true)

    IO.inspect(title)
    {:ok, socket}
  end

  defp page_settings(
         socket,
         page_title,
         header_big,
         header_background \\ "/images/header-image-mainfloor.jpg"
       ) do
    socket
    |> assign(
      query: "",
      page_title: page_title,
      header_big: header_big,
      header_background: header_background
    )
  end
end
