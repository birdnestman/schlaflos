defmodule SchlaflosWeb.Admin.MembershipLive.Index do
  use SchlaflosWeb, :live_view_admin

  alias Schlaflos.Memberships
  alias Schlaflos.Memberships.Membership

  @impl true
  def mount(_params, _session, socket) do
    records = list_records()

    socket =
      socket
      |> assign(:records, records)

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Membership")
    |> assign(:membership, Memberships.get_membership!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Membership")
    |> assign(:membership, %Membership{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Memberships")
    |> assign(:membership, nil)
  end

  defp list_records do
    Memberships.list_memberships()
  end
end
