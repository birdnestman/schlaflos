defmodule SchlaflosWeb.Admin.UserLive.Index do
  use SchlaflosWeb, :live_view_admin

  alias Schlaflos.Accounts
  alias Schlaflos.Accounts.User

  @impl true
  def mount(_params, _session, socket) do
    cols = [
      {"member_id", "Member ID"},
      {"full_name", "Name"},
      {"member_role", "Member Rolle"},
      {"member_end", "Member bis"},
      {"email", "E-Mail"},
      {"address", "Adresse"},
      {"city", "Wohnort"},
      {"role", "System Rolle"}
    ]

    users = list_users()

    show_cols =
      Enum.map(cols, fn {col, _} -> {col, "true"} end)
      |> Enum.into(%{})

    socket =
      socket
      |> assign(cols: cols, show_cols: show_cols)
      |> assign(:users, users)

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit User")
    |> assign(:user, Accounts.get_user!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New User")
    |> assign(:user, %User{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Users")
    |> assign(:user, nil)
  end

  defp list_users do
    Accounts.list_users()
  end

  def checked?(value) do
    case value do
      "true" -> "checked"
      "false" -> ""
    end
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    user = Accounts.get_user!(id)
    {:ok, _} = Accounts.delete_user(user)

    {:noreply, assign(socket, :users, list_users())}
  end

  def handle_event("show_cols", checked_cols, socket) do
    {:noreply, assign(socket, show_cols: checked_cols)}
  end

  def handle_event("search", %{"search_field" => %{"query" => query}}, socket) do
    if String.length(query) >= 2 do
      users = Accounts.list_users(query)
      {:noreply, assign(socket, :users, users)}
    else
      {:noreply, socket}
    end
  end
end
