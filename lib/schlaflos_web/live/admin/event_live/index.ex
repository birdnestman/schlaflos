defmodule SchlaflosWeb.Admin.EventLive.Index do
  use SchlaflosWeb, :live_view_admin

  alias Schlaflos.Events
  alias Schlaflos.Events.Event
  alias Schlaflos.Artists

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :events, list_events())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Event")
    |> assign(:event, Events.get_event!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Event")
    |> assign(:event, %Event{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Events")
    |> assign(:event, nil)
  end

  defp list_events do
    Events.list_events()
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    event = Events.get_event!(id)
    {:ok, _} = Events.delete_event(event)

    {:noreply, assign(socket, :events, list_events())}
  end

  def handle_event("suggest", %{"search" => search}, socket) do
    suggestions = Artists.list_artists() |> suggest(search)
    {:noreply, assign(socket, suggestions: suggestions)}
  end

  defp suggest(items, search) do
    Enum.filter(items, fn i ->
      i.name
      |> String.downcase()
      |> String.contains?(String.downcase(search))
    end)
  end
end
