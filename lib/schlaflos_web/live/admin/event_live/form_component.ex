defmodule SchlaflosWeb.Admin.EventLive.FormComponent do
  use SchlaflosWeb, :live_component

  alias Schlaflos.{Artists, Events, Events.Event}

  @impl true
  def mount(socket) do
    socket =
      socket
      |> assign(artist_suggestions: [])
      |> allow_upload(:live_image, accept: ~w(.png .jpeg .jpg), external: &presign_entry/2)

    {:ok, socket}
  end

  @impl true
  def update(%{event: event} = assigns, socket) do
    changeset = Events.change_event(event)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)
     |> assign(:artists_selected, Events.get_artists(event))}
  end

  def validate(socket, %{"event" => event_params}) do
    changeset =
      socket.assigns.event
      |> Events.change_event(event_params)
      |> Map.put(:action, :validate)

    assign(socket, :changeset, changeset)
  end

  @impl true
  def handle_event("change", params, socket) do
    socket =
      socket
      |> suggest_artists(params)
      |> validate(params)

    {:noreply, socket}
  end

  def handle_event("save", %{"event" => event_params}, socket) do
    save_event(socket, socket.assigns.action, event_params)
  end

  def handle_event("cancel-entry", %{"ref" => ref}, socket) do
    {:noreply, cancel_upload(socket, :image, ref)}
  end

  def handle_event("select", %{"id" => id}, socket) do
    artist = Artists.get_artist!(id)
    selected = Enum.uniq_by([artist] ++ socket.assigns.artists_selected, & &1.id)

    suggestions = filter_selected(socket.assigns.artist_suggestions, selected)

    socket =
      assign(socket,
        artists_selected: selected,
        artist_suggestions: suggestions
      )

    {:noreply, socket}
  end

  def handle_event("deselect-item", %{"id" => id}, socket) do
    artist_to_remove = Artists.get_artist!(id)

    socket =
      socket
      |> assign(:artists_selected, socket.assigns.artists_selected -- [artist_to_remove])

    {:noreply, socket}
  end

  def handle_event("submit", _params, socket), do: {:noreply, socket}

  defp save_event(socket, :edit, event_params) do
    # event_params = put_image_url(socket, event_params)

    case Events.update_event(
           socket.assigns.event,
           event_params,
           &consume_image(socket, &1),
           socket.assigns.artists_selected
         ) do
      {:ok, _event} ->
        {:noreply,
         socket
         |> put_flash(:info, "Event updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}

      _result ->
        {:noreply, assign(socket, "")}
    end
  end

  defp save_event(socket, :new, event_params) do
    case Events.create_event(
           %Event{},
           event_params,
           &consume_image(socket, &1),
           socket.assigns.artists_selected
         ) do
      {:ok, _event} ->
        {:noreply,
         socket
         |> put_flash(:info, "Event created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def ext(entry) do
    [ext | _] = MIME.extensions(entry.client_type)
    ext
  end

  defp put_image_url(socket, %Event{} = event) do
    {completed, []} = uploaded_entries(socket, :live_image)

    urls =
      for entry <- completed do
        Path.join(s3_host(), s3_key(entry))
      end

    %Event{event | image: List.first(urls)}
  end

  defp put_image_url(socket, params) do
    {completed, []} = uploaded_entries(socket, :live_image)

    urls =
      for entry <- completed do
        # Routes.static_path(socket, "/uploads/#{entry.uuid}.#{ext(entry)}")
        Path.join(s3_host(), s3_key(entry))
      end

    case urls do
      [] -> params
      _ -> Map.put(params, "image", List.first(urls))
    end
  end

  def consume_image(socket, %Event{} = event) do
    consume_uploaded_entries(socket, :live_image, fn _meta, entry ->
      name = event.name |> String.downcase() |> String.replace(~r"[ /]", "-")

      {:ok, image} =
        Schlaflos.Images.create_image(%Schlaflos.Images.Image{}, %{
          "name" => name,
          "uuid" => entry.uuid,
          "type" => ext(entry)
        })

      Events.add_image(event, image)
      :ok
    end)

    {:ok, event}
  end

  def suggest_artists(socket, %{"search" => ""}), do: assign(socket, :artist_suggestions, [])

  def suggest_artists(socket, %{"search" => search}) do
    suggestions = filter_selected(Artists.find_artists(search), socket.assigns.artists_selected)
    assign(socket, :artist_suggestions, suggestions)
  end

  defp filter_selected(items, selected) do
    Enum.filter(items, fn i -> !Enum.any?(selected, fn s -> i.id == s.id end) end)
  end

  @bucket "schlaflos"
  defp s3_host, do: "https://#{@bucket}.s3.amazonaws.com"
  defp s3_key(entry), do: "#{Mix.env()}/#{entry.uuid}/original.#{ext(entry)}"

  defp presign_entry(entry, socket) do
    uploads = socket.assigns.uploads
    key = s3_key(entry)

    config = %{
      scheme: "http://",
      host: "s3.amazonaws.com",
      region: "eu-central-1",
      access_key_id: System.fetch_env!("AWS_ACCESS_KEY_ID"),
      secret_access_key: System.fetch_env!("AWS_SECRET_ACCESS_KEY")
    }

    {:ok, fields} =
      Schlaflos.SimpleS3Upload.sign_form_upload(config, @bucket,
        key: key,
        content_type: entry.client_type,
        max_file_size: uploads.live_image.max_file_size,
        expires_in: :timer.hours(1)
      )

    meta = %{uploader: "S3", key: key, url: s3_host(), fields: fields}
    {:ok, meta, socket}
  end
end
