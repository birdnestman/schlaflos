defmodule SchlaflosWeb.Admin.EventLive.Show do
  use SchlaflosWeb, :live_view_admin

  use Timex

  alias Schlaflos.Events

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    event = Events.get_event!(id)

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:event, event)
     |> assign(:artists, Events.get_artists(event))}
  end

  defp page_title(:show), do: "Show Event"
  defp page_title(:edit), do: "Edit Event"

  def transform_date(:date, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{D}.{M}.{YYYY}")
  end

  def transform_date(:time, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{h24}:{m}")
  end
end
