defmodule SchlaflosWeb.Admin.MixLive.FormComponent do
  use SchlaflosWeb, :live_component

  alias Schlaflos.{Artists, Artists.Mix}

  @impl true
  def mount(socket) do
    socket =
      socket
      |> assign(all_artists: Artists.list_artists_for_select())
      |> allow_upload(:live_image, accept: ~w(.png .jpeg .jpg), external: &presign_entry/2)

    {:ok, socket}
  end

  @impl true
  def update(%{mix: mix} = assigns, socket) do
    changeset = Artists.change_mix(mix)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  def validate(socket, %{"mix" => event_params}) do
    changeset =
      socket.assigns.mix
      |> Artists.change_mix(event_params)
      |> Map.put(:action, :validate)

    assign(socket, :changeset, changeset)
  end

  @impl true
  def handle_event("change", params, socket) do
    socket =
      socket
      |> validate(params)

    {:noreply, socket}
  end

  def handle_event("save", %{"mix" => event_params}, socket) do
    save_event(socket, socket.assigns.action, event_params)
  end

  def handle_event("cancel-entry", %{"ref" => ref}, socket) do
    {:noreply, cancel_upload(socket, :image, ref)}
  end

  def handle_event("submit", _params, socket), do: {:noreply, socket}

  defp save_event(socket, :edit, event_params) do
    # event_params = put_image_url(socket, event_params)

    case Artists.update_mix(
           socket.assigns.mix,
           event_params,
           &consume_image(socket, &1)
         ) do
      {:ok, _event} ->
        {:noreply,
         socket
         |> put_flash(:info, "Mix updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}

      _result ->
        {:noreply, assign(socket, "")}
    end
  end

  defp save_event(socket, :new, event_params) do
    case Artists.create_mix(
           %Mix{},
           event_params,
           &consume_image(socket, &1)
         ) do
      {:ok, _event} ->
        {:noreply,
         socket
         |> put_flash(:info, "Mix created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def ext(entry) do
    [ext | _] = MIME.extensions(entry.client_type)
    ext
  end

  def consume_image(socket, %Mix{} = mix) do
    consume_uploaded_entries(socket, :live_image, fn _meta, entry ->
      name = mix.name |> String.downcase() |> String.replace(~r"[ /]", "-")

      {:ok, _image} =
        Schlaflos.Images.create_image(%Schlaflos.Images.Image{}, %{
          "name" => name,
          "uuid" => entry.uuid,
          "type" => ext(entry)
        })

      # Mixes.add_image(mix, image)
      :ok
    end)

    {:ok, mix}
  end

  @bucket "schlaflos"
  defp s3_host, do: "https://#{@bucket}.s3.amazonaws.com"
  defp s3_key(entry), do: "#{Mix.env()}/#{entry.uuid}/original.#{ext(entry)}"

  defp presign_entry(entry, socket) do
    uploads = socket.assigns.uploads
    key = s3_key(entry)

    config = %{
      scheme: "http://",
      host: "s3.amazonaws.com",
      region: "eu-central-1",
      access_key_id: System.fetch_env!("AWS_ACCESS_KEY_ID"),
      secret_access_key: System.fetch_env!("AWS_SECRET_ACCESS_KEY")
    }

    {:ok, fields} =
      Schlaflos.SimpleS3Upload.sign_form_upload(config, @bucket,
        key: key,
        content_type: entry.client_type,
        max_file_size: uploads.live_image.max_file_size,
        expires_in: :timer.hours(1)
      )

    meta = %{uploader: "S3", key: key, url: s3_host(), fields: fields}
    {:ok, meta, socket}
  end
end
