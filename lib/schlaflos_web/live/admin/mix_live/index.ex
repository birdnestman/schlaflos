defmodule SchlaflosWeb.Admin.MixLive.Index do
  use SchlaflosWeb, :live_view_admin

  alias Schlaflos.Artists.Mix
  alias Schlaflos.Artists

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :events, list_mixes())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Mix")
    |> assign(:mix, Artists.get_mix!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Mix")
    |> assign(:mix, %Mix{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Mixes")
    |> assign(:mix, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    mix = Artists.get_mix!(id)
    {:ok, _} = Artists.delete_mix(mix)

    {:noreply, assign(socket, :events, list_mixes())}
  end

  defp list_mixes do
    Artists.list_mixes()
  end
end
