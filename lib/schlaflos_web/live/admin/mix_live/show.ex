defmodule SchlaflosWeb.Admin.MixLive.Show do
  use SchlaflosWeb, :live_view_admin

  use Timex

  alias Schlaflos.Artists

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    mix = Artists.get_mix!(id)

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:mix, mix)}
  end

  defp page_title(:show), do: "Show Mix"
  defp page_title(:edit), do: "Edit Mix"

  def transform_date(:date, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{D}.{M}.{YYYY}")
  end

  def transform_date(:time, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{h24}:{m}")
  end
end
