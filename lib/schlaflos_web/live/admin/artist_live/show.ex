defmodule SchlaflosWeb.Admin.ArtistLive.Show do
  use SchlaflosWeb, :live_view_admin

  alias Schlaflos.Artists

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:artist, Artists.get_artist!(id))}
  end

  defp page_title(:show), do: "Show Artist"
  defp page_title(:edit), do: "Edit Artist"

  def handle_event("delete_image", _value, %{assigns: %{artist: artist}} = socket) do
    Artists.unset_image(artist)

    {:noreply, assign(socket, :artist, Artists.get_artist!(artist.slug))}
  end
end
