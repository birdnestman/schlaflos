defmodule SchlaflosWeb.Admin.ArtistLive.FormComponent do
  use SchlaflosWeb, :live_component

  alias Schlaflos.{Artists, Artists.Artist}

  @impl true
  def mount(socket) do
    {:ok,
     allow_upload(socket, :live_image,
       accept: ~w(.png .jpeg .jpg),
       max_entries: 1,
       external: &presign_entry/2
     )}
  end

  @impl true
  def update(%{artist: artist} = assigns, socket) do
    changeset = Artists.change_artist(artist)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"artist" => artist_params}, socket) do
    changeset =
      socket.assigns.artist
      |> Artists.change_artist(artist_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"artist" => artist_params}, socket) do
    save_artist(socket, socket.assigns.action, artist_params)
  end

  def handle_event("cancel-entry", %{"ref" => ref}, socket) do
    {:noreply, cancel_upload(socket, :live_image, ref)}
  end

  defp save_artist(socket, :edit, artist_params) do
    case Artists.update_artist(socket.assigns.artist, artist_params, &consume_image(socket, &1)) do
      {:ok, _artist} ->
        {:noreply,
         socket
         |> put_flash(:info, "Artist updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_artist(socket, :new, artist_params) do
    case Artists.create_artist(%Artist{}, artist_params, &consume_image(socket, &1)) do
      {:ok, _artist} ->
        {:noreply,
         socket
         |> put_flash(:info, "Artist created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def ext(entry) do
    [ext | _] = MIME.extensions(entry.client_type)
    ext
  end

  defp put_image_url(socket, %Artist{} = artist) do
    {completed, []} = uploaded_entries(socket, :live_image)

    urls =
      for entry <- completed do
        Path.join(s3_host(), s3_key(entry))
      end

    %Artist{artist | image: List.first(urls)}
  end

  defp put_image_url(socket, params) do
    {completed, []} = uploaded_entries(socket, :live_image)

    urls =
      for entry <- completed do
        # Routes.static_path(socket, "/uploads/#{entry.uuid}.#{ext(entry)}")
        Path.join(s3_host(), s3_key(entry))
      end

    case urls do
      [] -> params
      _ -> Map.put(params, "image", List.first(urls))
    end
  end

  def consume_image(socket, %Artist{} = artist) do
    consume_uploaded_entries(socket, :live_image, fn _meta, entry ->
      name = artist.name |> String.downcase() |> String.replace(~r"[ /]", "-")

      {:ok, image} =
        Schlaflos.Images.create_image(%Schlaflos.Images.Image{}, %{
          "name" => name,
          "uuid" => entry.uuid,
          "type" => ext(entry)
        })

      Artists.add_image(artist, image)
      :ok
    end)

    {:ok, artist}
  end

  @bucket "schlaflos"
  defp s3_host, do: "https://#{@bucket}.s3.amazonaws.com"
  defp s3_key(entry), do: "#{Mix.env()}/#{entry.uuid}/original.#{ext(entry)}"

  defp presign_entry(entry, socket) do
    uploads = socket.assigns.uploads
    key = s3_key(entry)

    config = %{
      scheme: "http://",
      host: "s3.amazonaws.com",
      region: "eu-central-1",
      access_key_id: System.fetch_env!("AWS_ACCESS_KEY_ID"),
      secret_access_key: System.fetch_env!("AWS_SECRET_ACCESS_KEY")
    }

    {:ok, fields} =
      Schlaflos.SimpleS3Upload.sign_form_upload(config, @bucket,
        key: key,
        content_type: entry.client_type,
        max_file_size: uploads.live_image.max_file_size,
        expires_in: :timer.hours(1)
      )

    meta = %{uploader: "S3", key: key, url: s3_host(), fields: fields}
    {:ok, meta, socket}
  end
end
