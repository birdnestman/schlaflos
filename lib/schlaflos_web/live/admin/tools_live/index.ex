defmodule SchlaflosWeb.Admin.ToolsLive.Index do
  use SchlaflosWeb, :live_view_admin

  alias Schlaflos.Images

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Tools")
    |> assign(:artist, nil)
  end

  @impl true
  def handle_event("delete", %{}, socket) do
    Schlaflos.Images.get_images()
    |> Enum.each(fn image ->
      image
      |> Images.update_versions(%{versions: %{}})
    end)

    {:noreply, socket}
  end
end
