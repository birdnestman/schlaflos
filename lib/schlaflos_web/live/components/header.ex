defmodule SchlaflosWeb.ComponentsLive.Header do
  use SchlaflosWeb, :live_component

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def get_title(%{page_title: title}) do
    title
  end

  def get_title(_) do
    "no title"
  end

  def get_header_background(%{header_background: background}), do: background
  def get_header_background(_), do: "/images/header-image-mainfloor.jpg"
end
