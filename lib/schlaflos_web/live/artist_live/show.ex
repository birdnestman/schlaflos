defmodule SchlaflosWeb.ArtistLive.Show do
  use SchlaflosWeb, :live_view

  alias Schlaflos.Artists
  alias Schlaflos.Repo

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    artist = Artists.get_artist!(id) |> Repo.preload(:image)

    meta_attrs = [
      %{name: "title", content: "Schlaflos Artist - #{artist.name}"},
      %{
        name: "description",
        content: "Erfahre wann «#{artist.name}» wieder im Schlaflos spielt."
      },
      %{property: "og:title", content: "Schlaflos Artist - #{artist.name}"},
      %{property: "og:image", content: Schlaflos.Images.get(artist.image, :medium)}
    ]

    {:noreply,
     socket
     |> assign(:meta_attrs, meta_attrs)
     |> page_settings(artist.name, false, Schlaflos.Images.get(artist.image, :large))
     |> assign(:artist, artist)
     |> assign(:events, Artists.get_events(artist))}
  end

  use Timex

  def transform_date(:date, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{D}.{M}.{YYYY}")
  end

  def transform_date(:time, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{h24}:{m}")
  end

  def transform_date(date) do
    Timezone.convert(date, "Europe/Zurich")
    |> Timex.format!("{WDshort} {D}. {Mshort} {YYYY}")
  end

  defp page_settings(
         socket,
         page_title,
         header_big,
         header_background
       ) do
    socket
    |> assign(
      query: "",
      page_title: page_title,
      header_big: header_big,
      header_background: header_background
    )
  end
end
