defmodule SchlaflosWeb.ArtistLive.Index do
  use SchlaflosWeb, :live_view

  alias Schlaflos.Artists

  @impl true
  def mount(_params, _session, socket) do
    title = "Artists und Performer"

    meta_attrs = [
      %{name: "title", content: title},
      %{
        name: "description",
        content:
          "Eine Auswahl von Events welche seit der Gründung im Schlaflos stattgefunden haben."
      },
      %{property: "og:title", content: "Schlaflos - #{title}"},
      %{
        property: "og:image",
        content: SchlaflosWeb.Router.Helpers.static_url(socket, "/images/schlaflos-seit-2014.jpg")
      }
    ]

    socket =
      socket
      |> assign(meta_attrs: meta_attrs)
      |> assign(:artists, list_artists())
      |> page_settings(title)

    {:ok, socket}
  end

  defp list_artists do
    Artists.list_artists_by_favorits()
  end

  defp page_settings(
         socket,
         page_title,
         header_big \\ false,
         header_background \\ "/images/header-image-mainfloor.jpg"
       ) do
    socket
    |> assign(
      query: "",
      page_title: page_title,
      header_big: header_big,
      header_background: header_background
    )
  end
end
