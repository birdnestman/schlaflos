defmodule SchlaflosWeb.EventLive.Show do
  use SchlaflosWeb, :live_view

  use Timex

  alias Schlaflos.Events
  alias Schlaflos.Repo

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    event = Events.get_event!(id) |> Repo.preload([:image])
    artists = Events.get_artists(event)

    meta_attrs = [
      %{name: "title", content: "Schlaflos - #{event.name}"},
      %{name: "description", content: event.description},
      %{property: "og:title", content: "Schlaflos - #{event.name}"},
      %{property: "og:image", content: Schlaflos.Images.get(event.image, :medium)}
    ]

    {:noreply,
     socket
     |> assign(:meta_attrs, meta_attrs)
     |> page_settings(event.name, false, Schlaflos.Images.get(event.image, :large))
     |> assign(:event, event)
     |> assign(:artists, artists)
     |> assign(:genres, Events.get_genres(artists))}
  end

  def print_from_to(start, duration) do
    from =
      start
      |> Timezone.convert("Europe/Zurich")
      |> Timex.format!("{WDshort} {D}. {Mshort} {YYYY} ({h24}:{m}")

    to =
      start
      |> Timezone.convert("Europe/Zurich")
      |> Timex.shift(hours: duration)
      |> Timex.format!("{h24}:{m}")

    "#{from} bis #{to})"
  end

  defp page_settings(
         socket,
         page_title,
         header_big,
         header_background
       ) do
    socket
    |> assign(
      query: "",
      page_title: page_title,
      header_big: header_big,
      header_background: header_background
    )
  end
end
