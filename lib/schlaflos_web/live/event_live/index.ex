defmodule SchlaflosWeb.EventLive.Index do
  use SchlaflosWeb, :live_view

  alias Schlaflos.Events

  @impl true
  def mount(_params, _session, socket) do
    title = "Alle Veranstaltungen"

    meta_attrs = [
      %{name: "title", content: title},
      %{name: "description", content: "Alle Events und Veranstaltungen vom Schlaflos-Club."},
      %{property: "og:title", content: "Schlaflos - #{title}"},
      %{
        property: "og:image",
        content: SchlaflosWeb.Router.Helpers.static_url(socket, "/images/schlaflos-seit-2014.jpg")
      }
    ]

    socket =
      socket
      |> assign(:meta_attrs, meta_attrs)
      |> assign(:events, Events.list_events())
      |> page_settings(title)

    {:ok, socket}
  end

  defp page_settings(
         socket,
         page_title,
         header_big \\ false,
         header_background \\ "/images/header-image-mainfloor.jpg"
       ) do
    socket
    |> assign(
      query: "",
      page_title: page_title,
      header_big: header_big,
      header_background: header_background
    )
  end
end
