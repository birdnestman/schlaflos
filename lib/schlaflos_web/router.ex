defmodule SchlaflosWeb.Router do
  use SchlaflosWeb, :router

  import SchlaflosWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {SchlaflosWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :xml do
    plug :put_root_layout, false
  end

  scope "/", SchlaflosWeb do
    pipe_through :api
    forward("/webhook", Plugs.WebhookShunt)
  end

  scope "/infoboard", SchlaflosWeb.Infoboard do
    live "/drinks", DrinksLive.Index, :index
    live "/events", EventsLive.Index, :index
  end

  scope "/admin", SchlaflosWeb.Admin, as: :admin do
    pipe_through [:browser, :require_authenticated_user]

    live "/", EventLive.Index, :index
    live "/events", EventLive.Index, :index
    live "/events/new", EventLive.Index, :new
    live "/events/:id/edit", EventLive.Index, :edit
    live "/events/:id", EventLive.Show, :show
    live "/events/:id/show/edit", EventLive.Show, :edit

    live "/mixes", MixLive.Index, :index
    live "/mixes/new", MixLive.Index, :new
    live "/mixes/:id/edit", MixLive.Index, :edit
    live "/mixes/:id", MixLive.Show, :show
    live "/mixess/:id/show/edit", MixLive.Show, :edit

    live "/artists", ArtistLive.Index, :index
    live "/artists/new", ArtistLive.Index, :new
    live "/artists/:id/edit", ArtistLive.Index, :edit

    live "/artists/:id", ArtistLive.Show, :show
    live "/artists/:id/show/edit", ArtistLive.Show, :edit

    live "/users", UserLive.Index, :index
    live "/users/new", UserLive.Index, :new
    live "/users/:id/edit", UserLive.Index, :edit

    live "/users/:id", UserLive.Show, :show
    live "/users/:id/show/edit", UserLive.Show, :edit
    live "/users/:id/show/letter", UserLive.Show, :letter

    scope "/memberships" do
      live "/", MembershipLive.Index, :index
      live "/new", MembershipLive.Index, :new
      live "/:id/edit", MembershipLive.Index, :edit
      live "/:id", MembershipLive.Show, :show
      live "/:id/show/edit", MembershipLive.Show, :edit
    end

    scope "/images" do
      live "/", ImageLive.Index, :index
      live "/new", ImageLive.Index, :new
      live "/:id/edit", ImageLive.Index, :edit
      live "/:id", ImageLive.Show, :show
      live "/:id/show/edit", ImageLive.Show, :edit
    end

    live "/tools", ToolsLive.Index, :index
  end

  scope "/", SchlaflosWeb do
    pipe_through :browser

    live "/", PageLive, :index
    live "/club", ClubLive.Index, :index
    live "/mitglied-werden", ClubLive.Membership, :membership
    live "/livestream", ClubLive.Livestream, :livestream
    live "/spenden", ClubLive.Spenden, :spenden
    live "/shop", ClubLive.Shop, :shop
    live "/artists", ArtistLive.Index, :index
    live "/artists/:id", ArtistLive.Show, :show
    live "/events", EventLive.Index, :index
    live "/events/:id", EventLive.Show, :show
    live "/summertanz", ClubLive.Summertanz, :summertanz

    resources "/pages", PageController
  end

  scope "/", SchlaflosWeb do
    pipe_through :xml

    get "/sitemap.xml", SitemapController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", SchlaflosWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: SchlaflosWeb.Telemetry
    end
  end

  ## Authentication routes

  scope "/", SchlaflosWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", SchlaflosWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings/update_password", UserSettingsController, :update_password
    put "/users/settings/update_email", UserSettingsController, :update_email
    put "/users/settings/update_profile", UserSettingsController, :update_profile
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
  end

  scope "/", SchlaflosWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :confirm
  end
end
