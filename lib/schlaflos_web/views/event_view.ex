defmodule SchlaflosWeb.EventView do
  use SchlaflosWeb, :view

  use Timex

  def transform_date(:date, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{D}.{M}.{YYYY}")
  end

  def transform_date(:time, date) do
    Timezone.convert(date, "Europe/Zurich") |> Timex.format!("{h24}:{m}")
  end

  def transform_date(date) do
    Timezone.convert(date, "Europe/Zurich")
    |> Timex.format!("{WDshort} {D}. {Mshort} {YYYY}")
  end
end
