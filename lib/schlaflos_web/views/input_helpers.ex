defmodule SchlaflosWeb.InputHelpers do
  use Phoenix.HTML

  def input(form, field, opts \\ []) do
    type = opts[:using] || Phoenix.HTML.Form.input_type(form, field)

    wrapper_opts = [class: "field"]
    label_opts = [class: "control-label"]
    input_opts = [class: "#{type_field_class(type)} #{state_class(form, field)}"]
    error_opts = [class: "help #{state_class(form, field)}"]

    input_element = input_element(type, form, field, input_opts, opts)

    error_element =
      content_tag :p, error_opts do
        error = SchlaflosWeb.ErrorHelpers.error_tag(form, field)
        error
      end

    content_tag :div, wrapper_opts do
      label = label(form, field, humanize(field), label_opts)
      [label, input_element, error_element]
    end
  end

  defp input_element(:select = type, form, field, input_opts, opts) do
    content_tag :div, class: "control" do
      content_tag :div, class: "select" do
        input = input(type, form, field, input_opts ++ opts)
        input
      end
    end
  end

  defp input_element(type, form, field, input_opts, opts) do
    content_tag :div, class: "control" do
      input = input(type, form, field, input_opts ++ opts)
      input
    end
  end

  defp state_class(form, field) do
    cond do
      # The form was not yet submitted
      is_nil(form.source.action) -> ""
      # The field has error
      form.errors[field] -> "is-danger"
      # The field is blank
      input_value(form, field) in ["", nil] -> ""
      # The field was filled successfully
      true -> "is-success"
    end
  end

  # Implement clauses below for custom inputs.
  # defp input(:datepicker, form, field, input_opts) do
  #   raise "not yet implemented"
  # end

  defp input(:select, form, field, input_opts) do
    apply(Phoenix.HTML.Form, :select, [form, field, Keyword.get(input_opts, :options)])
  end

  defp input(:checkbox, form, field, input_opts) do
    apply(Phoenix.HTML.Form, :select, [form, field, Keyword.get(input_opts, :options)])
  end

  defp input(:textarea, form, field, input_opts) do
    apply(Phoenix.HTML.Form, :textarea, [form, field, input_opts])
  end

  defp input(:datetime_select, form, field, input_opts) do
    apply(Phoenix.HTML.Form, :datetime_select, [
      form,
      field,
      input_opts ++ [year: [options: 2021..2030]]
    ])
  end

  defp input(type, form, field, input_opts) do
    apply(Phoenix.HTML.Form, type, [form, field, input_opts])
  end

  defp type_field_class(:text_input), do: "input"
  defp type_field_class(:textarea), do: "textarea"
  defp type_field_class(:datetime_select), do: "select"
  defp type_field_class(:select), do: "select"
  defp type_field_class(_), do: "input"
end
