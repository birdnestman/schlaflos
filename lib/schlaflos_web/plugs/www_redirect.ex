defmodule SchlaflosWeb.Plugs.WWWRedirect do
  import Plug.Conn

  def init(options) do
    options
  end

  def call(conn, _options) do
    if check_domain(conn.host) do
      conn
      |> Phoenix.Controller.redirect(external: www_url(conn))
      |> halt
    else
      # Since all plugs need to return a connection
      conn
    end
  end

  # Returns URL with www prepended for the given connection. Note this also
  # applies to hosts that already contain "www"
  defp www_url(conn) do
    "#{conn.scheme}://www.schlaflos-club.ch"
  end

  defp check_domain("0.0.0.0") do
    false
  end

  defp check_domain("neu.schlaflos-club.ch") do
    true
  end

  defp check_domain("schlaflos.fly.dev") do
    false
  end

  defp check_domain(host) do
    !Regex.match?(~r/\Awww\..*\z/i, host)
  end
end
