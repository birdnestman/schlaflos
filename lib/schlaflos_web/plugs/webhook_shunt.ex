defmodule SchlaflosWeb.Plugs.WebhookShunt do
  alias Plug.Conn

  alias SchlaflosWeb.WebhookRouter

  def init(opts), do: opts

  def call(%Conn{params: %{"eventName" => event}} = conn, opts) do
    conn
    |> change_path_info(["webhook", event])
    |> WebhookRouter.call(opts)
  end

  def call(%Conn{params: %{}} = conn, opts) do
    conn
    |> change_path_info(["webhook", "error"])
    |> WebhookRouter.call(opts)
  end

  def change_path_info(conn, new_path), do: put_in(conn.path_info, new_path)
end
