defmodule SchlaflosWeb.SitemapController do
  use SchlaflosWeb, :controller

  alias Schlaflos.{Artists, Events}

  def index(conn, _params) do
    events = Events.list_events()
    artists = Artists.list_artists()

    conn
    |> put_resp_content_type("text/xml")
    |> render("index.xml", events: events, artists: artists)
  end
end
