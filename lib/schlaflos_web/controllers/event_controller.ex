defmodule SchlaflosWeb.EventController do
  use SchlaflosWeb, :controller

  alias Schlaflos.Events

  def index(conn, _params) do
    events = Events.list_events()
    render(conn, "index.html", %{events: events, page_title: "Veranstaltungen"})
  end

  def show(conn, %{"id" => id}) do
    event = Events.get_event!(id)
    artists = Events.get_artists(event)

    render(conn, "show.html", %{
      artists: artists,
      event: event,
      header_background: event.image,
      page_title: event.name
    })
  end
end
