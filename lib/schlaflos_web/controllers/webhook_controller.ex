defmodule SchlaflosWeb.WebhookController do
  use SchlaflosWeb, :controller

  alias Schlaflos.SnipcartParser
  alias Schlaflos.{Accounts, Memberships}

  def order_completed(conn, params) do
    user = get_user(params)
    _items = SnipcartParser.parse_items(params)

    if is_paid(SnipcartParser.get_payment_status(params)) do
      create_membership(params, user)
    end

    json(conn, %{status: "ok"})
  end

  defp is_paid("Paid"), do: true
  defp is_paid("PaidDeferred"), do: true
  defp is_paid(_), do: false

  def order_status_changed(conn, params) do
    case SnipcartParser.parse_target_state(params) do
      "Cancelled" ->
        order_state(:cancelled, params)
        success(conn, params)

      "Processed" ->
        order_state(:processed, params)
        success(conn, params)

      "Shipped" ->
        order_state(:shipped, params)
        success(conn, params)

      "PaidDeferred" ->
        user = get_user(params)
        create_membership(params, user)
        order_state(:paid, params)
        success(conn, params)

      _ ->
        error(conn, params)
    end
  end

  def error(conn, _params) do
    json(conn, %{status: "error"})
  end

  def success(conn, _params) do
    json(conn, %{status: "ok"})
  end

  defp get_user(params) do
    params
    |> SnipcartParser.get_email()
    |> Accounts.get_user_by_email()
    |> case do
      nil -> Accounts.register_silent_user(SnipcartParser.parse_user(params))
      user -> user
    end
  end

  defp create_membership(params, user) do
    params
    |> SnipcartParser.has_membership?()
    |> case do
      true -> Memberships.create_silent_membership(user, SnipcartParser.parse_token(params))
      false -> false
    end
  end

  defp order_state(:cancelled, params) do
    params
    |> SnipcartParser.parse_token()
    |> Memberships.change_membership_state(%{active: false, state: "cancelled"})
  end

  defp order_state(:processed, params) do
    params
    |> SnipcartParser.parse_token()
    |> Memberships.change_membership_state(%{active: true, state: "processed"})
  end

  defp order_state(:shipped, params) do
    params
    |> SnipcartParser.parse_token()
    |> Memberships.change_membership_state(%{active: true, state: "shipped"})
  end
end
