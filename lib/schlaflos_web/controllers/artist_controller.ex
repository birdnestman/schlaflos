defmodule SchlaflosWeb.ArtistController do
  use SchlaflosWeb, :controller

  alias Schlaflos.Artists

  def index(conn, _params) do
    artists = Artists.list_artists()
    render(conn, "index.html", %{artists: artists, page_title: "Artists"})
  end

  def show(conn, %{"id" => id}) do
    artist = Artists.get_artist!(id)
    events = Artists.get_events(artist)
    render(conn, "show.html", %{artist: artist, events: events, page_title: artist.name})
  end
end
